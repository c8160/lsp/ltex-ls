# ltex

Grammar/Spell Checker Using LanguageTool with Support for LATEX, Markdown, and
Others.

> This image is tested for use with **podman** on **Linux** hosts with
> **x86_64** arch


## What is ltex?

LTEX provides offline grammar checking of various markup languages using
LanguageTool (LT). LTEX can be used standalone as a command-line tool, as a
language server using the Language Server Protocol (LSP), or directly in
various editors using extensions.

LTEX currently supports BibTEX, ConTEXt, LATEX, Markdown, Org,
reStructuredText, R Sweave, and XHTML documents.

See [the project website](https://valentjn.github.io/ltex/)


## How to use this image

This image is meant to be a one-to-one replacement of a natively installed
`ltex`. Being an LSP server, you most likely won't interact with `ltex`
directly. Instead, your editor of choice will connect to it to provide the
spell and grammar checking to you.

Since most LSP extensions by default search for an appropriately named
executable rather than connecting via TCP/IP, you should provide a shell
wrapper `ltex-ls` with the following contents:

```bash
#!/usr/bin/env bash

podman run --rm -i registry.gitlab.com/c8160/lsp/ltex-ls:latest $@
```

Make sure to place it somewhere on your path and make it executable. If
everything works, you can now execute `ltex-ls --help` and should see the help
text from `ltex`.

For the actual LSP integration, refer to the `ltex` documentation here:
https://valentjn.github.io/ltex/installation-usage.html

Or refer to your text managers help/documentation for integrating LSP servers.


## Getting help

First you may want to refer to the `help.md` contained in this repository or
the containers rootfs. If you're still stuck or found an issue with the
container in particular, feel free to open an issue on Gitlab:
https://gitlab.com/c8160/lsp/ltex-ls/-/issues

