#!/usr/bin/env bash
#
# Run as: "buildah unshare ./container.sh 'IMAGE_NAME'"
set -eu

# Query latest non-nightly from github
LTEX_VERSION="$(curl -Ss https://api.github.com/repos/valentjn/ltex-ls/releases/latest | grep -oP 'tag_name": "\K[0-9\.]+')"
LTEX_ARCHIVE="ltex-ls-$LTEX_VERSION-linux-x64.tar.gz"
BASE_IMAGE="registry.fedoraproject.org/fedora-minimal:39"

# Create a container
CONTAINER=$(buildah from "$BASE_IMAGE")
MOUNTPOINT=$(buildah mount $CONTAINER)

# Enter container FS
pushd "$MOUNTPOINT"
# Get the archive
curl -LOSs "https://github.com/valentjn/ltex-ls/releases/download/$LTEX_VERSION/$LTEX_ARCHIVE"
tar -xf "$LTEX_ARCHIVE"
rm "$LTEX_ARCHIVE"
popd

buildah unmount $CONTAINER

# Run ltex-ls
buildah config --entrypoint "$(printf '[ "/ltex-ls-%s/bin/ltex-ls" ]' "$LTEX_VERSION")" $CONTAINER
buildah config --cmd "" $CONTAINER

# Save the container to an image
buildah commit --squash $CONTAINER "$1"
