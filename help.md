ltex image, for grammar and spell checking.

Packages the [ltex Github project][1].

Documentation:
For the underlying ltex project, see https://github.com/valentjn/ltex-ls
For this container, see https://gitlab.com/c8160/lsp/ltex-ls

Requirements:
None

Configuration:
None


[1]: https://github.com/valentjn/ltex-ls
